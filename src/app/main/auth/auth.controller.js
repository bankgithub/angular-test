(function () {
    'use strict';

    angular
        .module('app.auth')
        .controller('LoginController', LoginController);

    function LoginController($state, Facebook, $scope, $rootScope) {
        var vm = this;

        vm.login = function () {
            // From now on you can use the Facebook service just as Facebook api says 
            Facebook.login(function (response) {
                // Do something with response. 
                console.log(response)
                vm.getLoginStatus();
            });

        };

        vm.getLoginStatus = function () {
            Facebook.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    $rootScope.loggedIn = true;
                    vm.me();
                    gotoProduct();

                } else {
                    $rootScope.loggedIn = false;
                }
            });
        };

        vm.me = function () {
            Facebook.api('/me', function (response) {

                console.log(response);
                var today = new Date();
                var isFirefox = typeof InstallTrigger !== 'undefined';
                if (isFirefox) {
                    $.cookie('bp_profile', JSON.stringify(response), { expires: 1 });
                } else {
                    document.cookie = "bp_profile=" + JSON.stringify(response) + "; expires=" + today
                }
                $rootScope.$broadcast('login', true);
               
            });
        };
        function gotoProduct() {
            $state.go('app.product');
        }
    }
})();
