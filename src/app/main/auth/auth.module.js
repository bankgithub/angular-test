(function ()
{
    'use strict';

    angular
        .module('app.auth', ['facebook'])
        .config(config);
       
    /** @ngInject */
    function config(FacebookProvider, $stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        FacebookProvider.init('1644556882305270');
        // State
        $stateProvider
            .state('app.auth', {
                url    : '/login',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/auth/login.html',
                        controller : 'LoginController as vm'
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/auth');


        // Navigation
        msNavigationServiceProvider.saveItem('auth', {
            title : 'AUTH',
            group : true,
            weight: 1,
            hidden: function(){
                return false;
            },
        });

        msNavigationServiceProvider.saveItem('auth.auth', {
            title    : 'Login',
            icon     : 'icon-login',
            state    : 'app.auth',
         
            weight   : 0,
            hidden: function(){
                return false;
            },
        });
    }
})();