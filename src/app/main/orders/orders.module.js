(function ()
{
    'use strict';

    angular
        .module('app.orders', [
            'datatables',
    
                'textAngular',
                'xeditable',
                'ui.bootstrap',
                 'datatables.buttons'
        ])
        .config(config);
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.replace(",", ";").split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    
        function isEmpty(obj) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop))
                    return false;
            }
    
            return true;
        }
    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.orders', {
                url    : '/orders',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/orders/orders.html',
                        controller : 'OrdersController as vm'
                    }
                },
                resolve: {
                    Orders: function (msApi)
                    {
                        return msApi.resolve('orders@get');
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/orders');

        // Api
        msApiProvider.register('orders', ['app/data/products/orders.json']);
       

        msNavigationServiceProvider.saveItem('home.orders', {
            title    : 'Orders',
            icon     : 'icon-cash-usd',
            state    : 'app.orders',
            weight   : 2,
            hidden: function(){
                var userJson = getCookie("bp_profile");
                return isEmpty(userJson);
            },
        });
    }
})();