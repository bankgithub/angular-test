(function () {
    'use strict';

    angular
        .module('app.product', [])
        .config(config);
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.replace(",", ";").split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    
        function isEmpty(obj) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop))
                    return false;
            }
    
            return true;
        }
    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('app.product', {
                url: '/product',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/product/product.html',
                        controller: 'ProductController as vm'
                    }
                },
                resolve: {
                    Products: function (msApi) {
                        return msApi.resolve('products@get');
                    },
                    Labels: function (LabelsService)
                    {
                        return LabelsService.getData();
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/product');

        // Api
        msApiProvider.register('products', ['app/data/notes/notes.json']);
       
        msApiProvider.register('product.labels', ['app/data/notes/labels.json']);

  // Navigation
        msNavigationServiceProvider.saveItem('home', {
            title : "HOME",
            group : true,
            weight: 1,
            hidden: function(){
                var userJson = getCookie("bp_profile");
                return isEmpty(userJson);
            },
        });

        msNavigationServiceProvider.saveItem('home.product', {
            title: 'Product',
            icon: 'icon-cart',
            state: 'app.product',

            weight: 1,
            hidden: function(){
                var userJson = getCookie("bp_profile");
                return isEmpty(userJson);
            },
        });
    }
})();