(function ()
{
    'use strict';

    angular
        .module('app.product')
        .factory('LabelsService', LabelsService);

    /** @ngInject */
    function LabelsService(msUtils, msApi, $q)
    {
        var service = {
            data       : [],
            addLabel   : addLabel,
            updateLabel: updateLabel,
            getData    : getData
        };

        /**
         * Add label
         * @param newLabel
         */
        function addLabel(newLabel)
        {
            if ( newLabel.name === '' )
            {
                return;
            }

            service.data.push({
                id   : msUtils.guidGenerator(),
                name : newLabel.name,
                color: newLabel.color || ''
            });
        }

        /**
         * Update Label
         * @param note
         */
        function updateLabel(note)
        {
            for ( var i = 0; i < service.data.length; i++ )
            {
                if ( service.data[i].id === note.id )
                {
                    service.data[i] = note;
                }
            }
        }

     

        /**
         * Get service data
         * @returns {Array}
         */
        function getData()
        {
            // Create a new deferred object
            var deferred = $q.defer();

            msApi.request('product.labels@get', {},
                // SUCCESS
                function (response)
                {
                    // Attach the data
                    service.data = response.data;

                    // Resolve the promise
                    deferred.resolve(response);
                },

                // ERROR
                function (response)
                {
                    // Reject the promise
                    deferred.reject(response);
                }
            );

            return deferred.promise;
        }

        return service;

    }
})();