(function ()
{
    'use strict';

    angular
        .module('app.toolbar', ['facebook'])
        .config(config);

    /** @ngInject */
    function config($translatePartialLoaderProvider)
    {
        $translatePartialLoaderProvider.addPart('app/toolbar');
    }
})();
